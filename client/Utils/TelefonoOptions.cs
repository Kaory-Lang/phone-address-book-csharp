using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MClient.Models;
using MClient.Utils;

namespace MClient.Utils;

public class TelefonoOptions {
	public static void telefono_options(int option) {
		if(option == 1) {
			Telefono telefono = new Telefono();
			string input;

			Console.Write("*Insert Telefono # (max: 10 chars): ");
			input = Console.ReadLine();
			telefono.Telefono1 = input == "" ? null : input;

			telefono.IdCliente = UtilsTools.get_int("*Insert IdCliente: ");
			string[] arguments = {telefono.IdCliente.ToString()};
			Console.WriteLine("Searching the Cliente");
			Cliente cliente = GenericApiAction<Cliente>.getRequest("api/Cliente", arguments).Result;

			if(cliente.IdCliente != telefono.IdCliente) {
				Console.WriteLine("ERROR!!! IdClient in Cliente doesn't exist");
				return;
			}

			Console.Write("\n*Insert Tipo Plan (max: 1 chars): ");
			input = Console.ReadLine();
			telefono.TipoPlan = input == "" ? null : input;
			string[] arguments2 = {telefono.TipoPlan};
			Console.WriteLine("Searching the Tipo Plan");
			Plane plan = GenericApiAction<Plane>.getRequest("api/Plane", arguments2).Result;

			if(plan.IdPlan != telefono.TipoPlan) {
				Console.WriteLine("ERROR!!! Tipo Plan in Planes doesn't exist");
				return;
			}

			GenericApiAction<Telefono>.postRequest("api/Telefono", telefono);
		} else if (option == 2) {
			List<Telefono> telefonos =
				GenericApiAction<Telefono>.getListRequest("api/Telefono").Result;

			if(telefonos.Count != 0) {
				foreach(Telefono telefono in telefonos) {
					GenericApiAction<Telefono>.printProperties(telefono);
				}
			}
		} else if (option == 3) {
			Console.Write("Insert Plan ID (max: 1 char): ");
			string id = Console.ReadLine();
			string[] arguments = {id};
			Telefono telefono =
				GenericApiAction<Telefono>.getRequest("api/Telefono", arguments).Result;

			GenericApiAction<Telefono>.printProperties(telefono);
		} else if (option == 4) {
			Telefono telefono = new Telefono();
			string input;

			Console.Write("*Insert Telefono # to sustitute (max: 10 chars): ");
			input = Console.ReadLine();
			telefono.Telefono1 = input == "" ? null : input;

			telefono.IdCliente = UtilsTools.get_int("*Insert IdCliente: ");
			string[] arguments = {telefono.IdCliente.ToString()};
			Console.WriteLine("Searching the Cliente");
			Cliente cliente = GenericApiAction<Cliente>.getRequest("api/Cliente", arguments).Result;

			if(cliente.IdCliente != telefono.IdCliente) {
				Console.WriteLine("ERROR!!! IdClient in Cliente doesn't exist");
				return;
			}

			Console.Write("\n*Insert Tipo Plan (max: 1 chars): ");
			input = Console.ReadLine();
			telefono.TipoPlan = input == "" ? null : input;
			string[] arguments2 = {telefono.TipoPlan};
			Console.WriteLine("Searching the Tipo Plan");
			Plane plan = GenericApiAction<Plane>.getRequest("api/Plane", arguments2).Result;

			if(plan.IdPlan != telefono.TipoPlan) {
				Console.WriteLine("ERROR!!! Tipo Plan in Planes doesn't exist");
				return;
			}

			GenericApiAction<Telefono>.putRequest("api/Telefono", telefono, telefono.Telefono1);
		} else if (option == 5) {
			Console.Write("Insert the id of the row to delete (max: 10 chars): ");
			string id = Console.ReadLine();

			GenericApiAction<Telefono>.deleteRequest("api/Telefono", id);
		} else if (option == 0) {
			Console.Clear();
			return;
		}
	}
}
