using System;
using System.IO;
using System.Collections.Generic;
using MClient.Models;

namespace MClient.Utils;

public class UtilsTools {
	public static string get_crud_interface(string tableName) {
		Console.Clear();

		string result = "";

		result += $"-- CRUD {tableName} Options:";
		result += $"\n\t[1] CREATE {tableName}";
		result += $"\n\t[2] READ ALL {tableName}";
		result += $"\n\t[3] READ {{id}} {tableName}";
		result += $"\n\t[4] UPDATE {tableName}";
		result += $"\n\t[5] DELETE {tableName}";
		result += $"\n\t[0] Back to Menu\n";

		return result;
	}

	public static int get_int(string labelMsg = "Select an Option >> ") {
		bool isInt = false;

		while(!isInt) {
			int selectedOption = 0;

			Console.Write(labelMsg);

			try {
				selectedOption = Convert.ToInt32(Console.ReadLine());
				isInt = true;
			} catch (Exception e) { continue; }

			Console.Write("\n");
			return selectedOption;
		}

		return 0;
	}

	public static void tables_to_csv() {
		string clienteHeader = "IdCliente,Nombre,Apellido,FechaNacimiento";
		string planesHeader = "IdPlan,Descripcion,Renta,CostoMin";
		string llamadasHeader = "CodLlamda,Telefono,Fecha,Duracion";
		string telefonoHeader = "Telefono,IdClient,TipoPlan";

		using(StreamWriter sw = new StreamWriter("./CSVTables/cliente-table.csv", true)) {
			Console.WriteLine("Downloading Cliente table");
			List<Cliente> clientes =
				GenericApiAction<Cliente>.getListRequest("api/Cliente").Result;

			Console.WriteLine("Saving Cliente table... Wait until end");
			sw.WriteLine(clienteHeader);
			foreach(Cliente c in clientes)
				sw.WriteLine($"{c.IdCliente},{c.Nombre},{c.Apellido},{c.FechaNacimiento}");

			Console.WriteLine("Successfully saved.");
		}

		using(StreamWriter sw = new StreamWriter("./CSVTables/planes-table.csv", true)) {
			Console.WriteLine("\nSaving Planes table");
			List<Plane> planes =
				GenericApiAction<Plane>.getListRequest("api/Plane").Result;

			Console.WriteLine("Saving Planes table... Wait until end");
			sw.WriteLine(planesHeader);
			foreach(Plane p in planes)
				sw.WriteLine($"{p.IdPlan},{p.Descripcion},{p.Renta},{p.CostoMin}");

			Console.WriteLine("Successfully saved.");
		}

		using(StreamWriter sw = new StreamWriter("./CSVTables/llamadas-table.csv", true)) {
			Console.WriteLine("\nDownloading Llamadas table");
			List<Llamada> llamadas =
				GenericApiAction<Llamada>.getListRequest("api/Llamada").Result;

			Console.WriteLine("Saving Llamadas table... Wait until end");
			sw.WriteLine(llamadasHeader);
			foreach(Llamada l in llamadas)
				sw.WriteLine($"{l.CodLlamada},{l.Telefono},{l.Fecha},{l.Duracion}");

			Console.WriteLine("Successfully saved.");
		}

		using(StreamWriter sw = new StreamWriter("./CSVTables/telefono-table.csv", true)) {
			Console.WriteLine("\nDownloading Telefono table");
			List<Telefono> telefonos =
				GenericApiAction<Telefono>.getListRequest("api/Telefono").Result;

			Console.WriteLine("Saving Telefono table... Wait until end");
			sw.WriteLine(telefonoHeader);
			foreach(Telefono t in telefonos)
				sw.WriteLine($"{t.Telefono1},{t.IdCliente},{t.TipoPlan}");

			Console.WriteLine("Successfully saved.");
		}
	}

	public static void print_LlamadasXCliente() {
		List<Cliente> clientes =
			GenericApiAction<Cliente>.getListRequest("api/LlamadaXCliente").Result;

		foreach(Cliente c in clientes) {
			Console.WriteLine($"[{c.IdCliente}] {c.Nombre} {c.Apellido}");

			foreach(Telefono t in c.Telefonos) {
				Console.WriteLine($"\tTelefono: {t.Telefono1}");

				Console.WriteLine($"\t\tLlamadas:");
				foreach(Llamada llamada in t.Llamada) {
					Console.WriteLine($"\t\t\tCod Llamada: {llamada.CodLlamada}");
					Console.WriteLine($"\t\t\tTelefono #: {llamada.Telefono}");
					Console.WriteLine($"\t\t\tDuracion: {llamada.Duracion}");
					Console.WriteLine($"\t\t\tFecha: {llamada.Fecha}\n");
				}
			}
		}
	}

	public static void charge_telefono_file() {
		Console.WriteLine("Insert the path of the Telefono file to charge ");
		Console.WriteLine("Example: \"./path/to/the/file.csv\"");
		Console.Write("\nPath >> ");
		string path = Console.ReadLine();

		TextWriter backupOut = Console.Out;
		List<string[]> lines = new List<string[]>();
		using(StreamReader sr = new StreamReader(path)) {
			string line = "";

			while((line = sr.ReadLine()) != null)
				lines.Add(line.Split(','));

			lines.RemoveAt(0);
		}

		Console.WriteLine("Saving telefonos... Wait until end");
		int lineNumber = 0;
		foreach(String[] line in lines) {
			Console.SetOut(TextWriter.Null);
			Telefono telefono = new Telefono();

			telefono.Telefono1 = line[0];
			telefono.IdCliente = Convert.ToInt32(line[1]);

			string[] arguments = {telefono.IdCliente.ToString()};
			Cliente cliente = GenericApiAction<Cliente>.getRequest("api/Cliente", arguments).Result;

			if(cliente.IdCliente != telefono.IdCliente) {
				Console.SetOut(backupOut);
				Console.WriteLine("ERROR!!! IdClient in Cliente doesn't exist");
				return;
			}

			telefono.TipoPlan = line[2];

			string[] arguments2 = {telefono.TipoPlan};
			Plane plan = GenericApiAction<Plane>.getRequest("api/Plane", arguments2).Result;

			if(plan.IdPlan != telefono.TipoPlan) {
				Console.SetOut(backupOut);
				Console.WriteLine("ERROR!!! Tipo Plan in Planes doesn't exist");
				return;
			}

			bool statusOk =
				GenericApiAction<Telefono>.postRequest("api/Telefono", telefono);

			Console.SetOut(backupOut);
			if(!statusOk) {
				Console.Write($"\nERROR!!! File row number [#{lineNumber}] ");
				Console.WriteLine("Something went wrong doing the POST request.");
				return;
			}

			lineNumber++;
		}
		Console.WriteLine("Task finished\n");
	}
}
