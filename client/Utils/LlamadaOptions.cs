using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MClient.Models;

namespace MClient.Utils;

public class LlamadaOptions {
	public static void llamada_options(int option) {
		if(option == 1) {
			Llamada llamada = new Llamada();

			llamada.Duracion = UtilsTools.get_int("*Insert Duracion: ");

			Console.Write("*Insert Telefono #: ");
			llamada.Telefono = Console.ReadLine();
			string[] arguments = {llamada.Telefono};
			Console.WriteLine("Searching if Telefono exist");
			Telefono telefono =
				GenericApiAction<Telefono>.getRequest("api/Telefono", arguments).Result;

			if(telefono.Telefono1 != llamada.Telefono) {
				Console.WriteLine("ERROR!!! Telefono in Telefono table doesn't exist");
				return;
			}

			Console.Write("\n*Insert Fecha [YYYY-MM-DD]: ");
			string[] date = Console.ReadLine().Split('-');

			try {
				int year = Convert.ToInt32(date[0]);
				int month = Convert.ToInt32(date[1]);
				int day = Convert.ToInt32(date[2]);

				llamada.Fecha = new DateTime(year, month, day);
			} catch (Exception e) {
				Console.WriteLine("ERROR!!! Insert valid 'Fecha' format.");
			}
			
			GenericApiAction<Llamada>.postRequest("api/Llamada", llamada);
		} else if (option == 2) {
			List<Llamada> llamadas =
				GenericApiAction<Llamada>.getListRequest("api/Llamada").Result;

			if(llamadas.Count != 0) {
				foreach(Llamada llamada in llamadas) {
					GenericApiAction<Llamada>.printProperties(llamada);
				}
			}
		} else if (option == 3) {
			int cId = UtilsTools.get_int("Insert Cod Llamada: ");
			string[] arguments = {cId.ToString()};
			Llamada llamada =
				GenericApiAction<Llamada>.getRequest("api/Llamada", arguments).Result;

			GenericApiAction<Llamada>.printProperties(llamada);
		} else if (option == 4) {
			Llamada llamada = new Llamada();
			int id = UtilsTools.get_int("Insert CodTelefono to sustitute: ");
			string input;

			llamada.CodLlamada = id;

			llamada.Duracion = UtilsTools.get_int("Insert Duration: ");

			Console.Write("Insert Phone #: ");
			llamada.Telefono = Console.ReadLine();
			string[] arguments = {llamada.Telefono};
			Console.WriteLine("Searching if Telefono exist");
			Telefono telefono =
				GenericApiAction<Telefono>.getRequest("api/Telefono", arguments).Result;

			if(telefono.Telefono1 != llamada.Telefono) {
				Console.WriteLine("ERROR!!! Telefono in Telefono table doesn't exist");
				return;
			}

			Console.Write("\nFecha de Nacimiento [YYYY-MM-DD]: ");
			string[] date = Console.ReadLine().Split('-');

			try {
				int year = Convert.ToInt32(date[0]);
				int month = Convert.ToInt32(date[1]);
				int day = Convert.ToInt32(date[2]);

				llamada.Fecha = new DateTime(year, month, day);
			} catch (Exception e) {
				Console.WriteLine("ERROR!!! Insert valid 'Fecha' format.");
			}

			GenericApiAction<Llamada>.putRequest("api/Llamada", llamada, id.ToString());
		} else if (option == 5) {
			int id = UtilsTools.get_int("Insert the id of the row to delete >> ");
			GenericApiAction<Llamada>.deleteRequest("api/Llamada", id.ToString());
		} else if (option == 0) {
			Console.Clear();
			return;
		}
	}
}
