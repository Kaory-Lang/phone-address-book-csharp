using System;
using System.Text;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using MClient.Models;

namespace MClient.Utils;

public class GenericApiAction<T> {
	public static void printProperties(T entity) {
		if(entity.ToString() == "MClient.Models.Cliente") {
			Cliente cliente = entity as Cliente;
			Console.WriteLine($"\nCliente ID: {cliente.IdCliente}");
			Console.WriteLine($"Nombre: {cliente.Nombre}");
			Console.WriteLine($"Apellido: {cliente.Apellido}");
			Console.WriteLine($"Fecha de Nacimiento: {cliente.FechaNacimiento}");
		}

		if(entity.ToString() == "MClient.Models.Llamada") {
			Llamada llamada = entity as Llamada;
			Console.WriteLine($"\nCod Llamada: {llamada.CodLlamada}");
			Console.WriteLine($"Telefono #: {llamada.Telefono}");
			Console.WriteLine($"Duracion: {llamada.Duracion}");
			Console.WriteLine($"Fecha: {llamada.Fecha}");
		}

		if(entity.ToString() == "MClient.Models.Plane") {
			Plane plan = entity as Plane;
			Console.WriteLine($"\nIdPlan: {plan.IdPlan}");
			Console.WriteLine($"Descripcion #: {plan.Descripcion}");
			Console.WriteLine($"Renta: {plan.Renta}");
			Console.WriteLine($"CostoMin: {plan.CostoMin}");
		}

		if(entity.ToString() == "MClient.Models.Telefono") {
			Telefono telefono = entity as Telefono;
			Console.WriteLine($"\nTelefono1: {telefono.Telefono1}");
			Console.WriteLine($"IdCliente #: {telefono.IdCliente}");
			Console.WriteLine($"TipoPlan: {telefono.TipoPlan}");
		}
	}

	public static async Task<List<T>> getListRequest(string endpoint) {
		string url = $"http://localhost:5113/{endpoint}";

		Console.WriteLine("Wait until complete...");

		using(HttpClient client = new HttpClient()) {
			HttpResponseMessage response = await client.GetAsync(url);

			if(!response.IsSuccessStatusCode) {
				Console.WriteLine("ERROR!!! Could't get the data from the database");
				return new List<T>();
			}

			Console.WriteLine("Action completed successfully");

			string responseBody = await response.Content.ReadAsStringAsync();

			return JsonConvert.DeserializeObject<List<T>>(responseBody);
		}
	} 

	public static async Task<T> getRequest(string endpoint, string[] parameters) {
		string url = $"http://localhost:5113/{endpoint}/";

		foreach(string parameter in parameters)
			url += parameter + "/";

		Console.WriteLine("Wait until complete...");

		using(HttpClient client = new HttpClient()) {
		 	HttpResponseMessage response = await client.GetAsync(url);

			if(!response.IsSuccessStatusCode) {
				Console.WriteLine("\nERROR!!! Could't get the data from the database");

				T obj = default(T);
				obj = Activator.CreateInstance<T>();
				return obj;
			}

			Console.WriteLine("Action completed successfully");

		 	string responseBody = await response.Content.ReadAsStringAsync();

		 	return JsonConvert.DeserializeObject<T>(responseBody);
		}
	}

	public static bool postRequest(string endpoint, T entity) {
		string json = JsonConvert.SerializeObject(entity);
		StringContent data = new StringContent(json, Encoding.UTF8, "application/json");

		//Console.WriteLine(json);
		Console.WriteLine("Wait until complete...");

		using(HttpClient client = new HttpClient()) {
			Task<HttpResponseMessage> response =
				client.PostAsync($"http://localhost:5113/{endpoint}", data);

			if(!response.Result.IsSuccessStatusCode) {
				Console.WriteLine("\nERROR!!! Error in the registering");
				Console.WriteLine("Review no repetition of the PK, values no nullable or incorrect data types");
				return false;
			};

			Console.WriteLine("Action completed successfully");
		}

		return true;
	}

	public static bool putRequest(string endpoint, T entity, string id) {
		string json = JsonConvert.SerializeObject(entity);
		StringContent data = new StringContent(json, Encoding.UTF8, "application/json");

		//Console.WriteLine(json);
		Console.WriteLine("Wait until complete...");

		using(HttpClient client = new HttpClient()) {
			Task<HttpResponseMessage> response =
				client.PutAsync($"http://localhost:5113/{endpoint}/{id}", data);

			if(!response.Result.IsSuccessStatusCode) {
				Console.WriteLine("\nERROR!!! Error in the updating");
				Console.WriteLine("Review no equal PK, values no nullable or incorrect data types");
				return false;
			};

			Console.WriteLine("Action completed successfully");
		}
		
		return true;
	}

	public static void deleteRequest(string endpoint, string id) {
		Console.WriteLine("Wait until complete...");

		using(HttpClient client = new HttpClient()) {
			Task<HttpResponseMessage> response =
				client.DeleteAsync($"http://localhost:5113/{endpoint}/{id}");

			if(!response.Result.IsSuccessStatusCode) {
				Console.WriteLine("\nERROR!!! Error deleting");
				Console.WriteLine("Review id exist");
				return;
			};

			Console.WriteLine("Action completed successfully");
		}
	}
}
