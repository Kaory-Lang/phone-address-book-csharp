using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MClient.Models;

namespace MClient.Utils;

public class PlanesOptions {
	public static void planes_options(int option) {
		if(option == 1) {
			Plane plan = new Plane();
			string input;

			Console.Write("*Insert Plan ID (max: 1 char): ");
			input = Console.ReadLine();
			plan.IdPlan = input == "" ? null : input;

			Console.Write("*Insert plan description (max: 10 chars): ");
			input = Console.ReadLine();
			plan.Descripcion = input == "" ? null : input;

			plan.Renta = UtilsTools.get_int("*Insert renta of the plan: ");
			plan.CostoMin = UtilsTools.get_int("*Insert CostoMin of the plan: ");

			GenericApiAction<Plane>.postRequest("api/Plane", plan);
		} else if (option == 2) {
			List<Plane> planes =
				GenericApiAction<Plane>.getListRequest("api/Plane").Result;

			if(planes.Count != 0) {
				foreach(Plane plan in planes) {
					GenericApiAction<Plane>.printProperties(plan);
				}
			}
		} else if (option == 3) {
			Console.Write("Insert Plan ID (max: 1 char): ");
			string id = Console.ReadLine();
			string[] arguments = {id};
			Plane plan =
				GenericApiAction<Plane>.getRequest("api/Plane", arguments).Result;

			GenericApiAction<Plane>.printProperties(plan);
		} else if (option == 4) {
			Plane plan = new Plane();
			string input;

			Console.Write("*Insert Plan ID to sustite (max: 1 char): ");
			input = Console.ReadLine();
			plan.IdPlan = input == "" ? null : input;

			Console.Write("*Insert plan description (max: 10 chars): ");
			input = Console.ReadLine();
			plan.Descripcion = input == "" ? null : input;

			plan.Renta = UtilsTools.get_int("*Insert renta of the plan: ");
			plan.CostoMin = UtilsTools.get_int("*Insert CostoMin of the plan: ");

			GenericApiAction<Plane>.putRequest("api/Plane", plan, plan.IdPlan);
		} else if (option == 5) {
			Console.Write("Insert the id of the row to delete (max: 1 char): ");
			string id = Console.ReadLine();

			GenericApiAction<Plane>.deleteRequest("api/Plane", id);
		} else if (option == 0) {
			Console.Clear();
			return;
		}
	}
}
