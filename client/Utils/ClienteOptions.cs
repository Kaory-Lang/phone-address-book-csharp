using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MClient.Models;

namespace MClient.Utils;

public class ClienteOptions {
	public static void cliente_options(int option) {
		if(option == 1) {
			Cliente cliente = new Cliente();

			cliente.IdCliente = UtilsTools.get_int("Insert ID: ");
			Console.Write("Insert Nombre: ");
			cliente.Nombre = Console.ReadLine();
			Console.Write("\nInsert Apellido: ");
			cliente.Apellido = Console.ReadLine();
			Console.Write("\nFecha de Nacimiento [YYYY-MM-DD]: ");
			string[] date = Console.ReadLine().Split('-');

			try {
				int year = Convert.ToInt32(date[0]);
				int month = Convert.ToInt32(date[1]);
				int day = Convert.ToInt32(date[2]);

				cliente.FechaNacimiento = new DateTime(year, month, day);
			} catch (Exception e) {
				Console.WriteLine("WARNING!!! Insert valid date format");
			}

			GenericApiAction<Cliente>.postRequest("api/Cliente", cliente);
		} else if (option == 2) {
			List<Cliente> clientes =
				GenericApiAction<Cliente>.getListRequest("api/Cliente").Result;

			if(clientes.Count != 0) {
				foreach(Cliente cliente in clientes) {
					GenericApiAction<Cliente>.printProperties(cliente);
				}
			}
		} else if (option == 3) {
			int cId = UtilsTools.get_int("Insert client Id >> ");
			string[] arguments = {cId.ToString()};
			Cliente cliente =
				GenericApiAction<Cliente>.getRequest("api/Cliente", arguments).Result;

			GenericApiAction<Cliente>.printProperties(cliente);
		} else if (option == 4) {
			Cliente cliente = new Cliente();
			int id = UtilsTools.get_int("Insert client Id to sustitute >> ");
			string input;

			cliente.IdCliente = id;

			Console.Write("Insert Nombre: ");
			input = Console.ReadLine();
			cliente.Nombre = input == "" ? null : input;

			Console.Write("\nInsert Apellido: ");
			input = Console.ReadLine();
			cliente.Apellido = input == "" ? null : input;

			Console.Write("\nFecha de Nacimiento [YYYY-MM-DD]: ");
			string[] date = Console.ReadLine().Split('-');

			try {
				int year = Convert.ToInt32(date[0]);
				int month = Convert.ToInt32(date[1]);
				int day = Convert.ToInt32(date[2]);

				cliente.FechaNacimiento = new DateTime(year, month, day);
			} catch (Exception e) {
				Console.WriteLine("NOTE!!! Date will be null");
			}

			GenericApiAction<Cliente>.putRequest("api/Cliente", cliente, id.ToString());
		} else if (option == 5) {
			int id = UtilsTools.get_int("Insert the id of the row to delete >> ");
			GenericApiAction<Cliente>.deleteRequest("api/Cliente", id.ToString());
		} else if (option == 0) {
			Console.Clear();
			return;
		}
	}
}
