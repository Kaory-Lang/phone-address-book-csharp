﻿using System;
using System.Collections.Generic;

namespace MClient.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            Telefonos = new HashSet<Telefono>();
        }

        public int IdCliente { get; set; }
        public string? Nombre { get; set; }
        public string? Apellido { get; set; }
        public DateTime? FechaNacimiento { get; set; }

        public virtual ICollection<Telefono> Telefonos { get; set; }
    }
}
