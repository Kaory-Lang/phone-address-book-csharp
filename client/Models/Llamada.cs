﻿using System;
using System.Collections.Generic;

namespace MClient.Models
{
    public partial class Llamada
    {
        public int CodLlamada { get; set; }
        public string Telefono { get; set; } = null!;
        public DateTime Fecha { get; set; }
        public int Duracion { get; set; }

        public virtual Telefono? TelefonoNavigation { get; set; }
    }
}
