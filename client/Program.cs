﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MClient.Models;
using MClient.Utils;
using System.IO;

namespace MClient;

internal class Program {
	public static void Main(string[] args) {
		Console.WriteLine("\n****** Welcome to BD Telefonia Interface ******\n");

		while(true) {
			Console.WriteLine("-- Menu:");
			Console.WriteLine("\t[1] CRUD Cliente");
			Console.WriteLine("\t[2] CRUD Planes");
			Console.WriteLine("\t[3] CRUD Telefono");
			Console.WriteLine("\t[4] CRUD Llamadas");
			Console.WriteLine("\t[5] Export all tables as .CSV File");
			Console.WriteLine("\t[6] Report Llamadas X Client");
			Console.WriteLine("\t[7] Charge Phone File");
			Console.WriteLine("\t[0] Exit Program\n");

			int option = UtilsTools.get_int();

			if(option == 1) {
				Console.WriteLine(UtilsTools.get_crud_interface("Cliente"));
				ClienteOptions.cliente_options(UtilsTools.get_int());
			} else if(option == 2) {
				Console.WriteLine(UtilsTools.get_crud_interface("Planes"));
				PlanesOptions.planes_options(UtilsTools.get_int());
			} else if(option == 3) {
				Console.WriteLine(UtilsTools.get_crud_interface("Telefono"));
				TelefonoOptions.telefono_options(UtilsTools.get_int());
			} else if(option == 4) {
				Console.WriteLine(UtilsTools.get_crud_interface("Llamadas"));
				LlamadaOptions.llamada_options(UtilsTools.get_int());
			} else if(option == 5) {
				UtilsTools.tables_to_csv();
			} else if(option == 6) {
				UtilsTools.print_LlamadasXCliente();
			} else if(option == 7) {
				UtilsTools.charge_telefono_file();
			} else if(option == 0) {
				Environment.Exit(0);
			}
		}
	}
}
