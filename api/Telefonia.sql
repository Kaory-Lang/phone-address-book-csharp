USE [master]
GO
/****** Object:  Database [Telefonia]    Script Date: 06/22/2020 18:08:15 ******/
CREATE DATABASE [Telefonia]
GO
ALTER DATABASE [Telefonia] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Telefonia].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Telefonia] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [Telefonia] SET ANSI_NULLS OFF
GO
ALTER DATABASE [Telefonia] SET ANSI_PADDING OFF
GO
ALTER DATABASE [Telefonia] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [Telefonia] SET ARITHABORT OFF
GO
ALTER DATABASE [Telefonia] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [Telefonia] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [Telefonia] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [Telefonia] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [Telefonia] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [Telefonia] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [Telefonia] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [Telefonia] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [Telefonia] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [Telefonia] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [Telefonia] SET  DISABLE_BROKER
GO
ALTER DATABASE [Telefonia] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [Telefonia] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [Telefonia] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [Telefonia] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [Telefonia] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [Telefonia] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [Telefonia] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [Telefonia] SET  READ_WRITE
GO
ALTER DATABASE [Telefonia] SET RECOVERY SIMPLE
GO
ALTER DATABASE [Telefonia] SET  MULTI_USER
GO
ALTER DATABASE [Telefonia] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [Telefonia] SET DB_CHAINING OFF
GO
USE [Telefonia]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 06/22/2020 18:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[IdCliente] [int] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[FechaNacimiento] [date] NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Cliente] ([IdCliente], [Nombre], [Apellido], [FechaNacimiento]) VALUES (1001, N'Juan', N'Perez', CAST(0x21190B00 AS Date))
INSERT [dbo].[Cliente] ([IdCliente], [Nombre], [Apellido], [FechaNacimiento]) VALUES (1002, N'Luis', N'Matos', CAST(0x5C110B00 AS Date))
INSERT [dbo].[Cliente] ([IdCliente], [Nombre], [Apellido], [FechaNacimiento]) VALUES (1003, N'Minerva', N'Amancio', CAST(0x00DD0A00 AS Date))
INSERT [dbo].[Cliente] ([IdCliente], [Nombre], [Apellido], [FechaNacimiento]) VALUES (1004, N'Angela', N'Zapata', CAST(0xA10E0B00 AS Date))
INSERT [dbo].[Cliente] ([IdCliente], [Nombre], [Apellido], [FechaNacimiento]) VALUES (1005, N'Silvestre', N'Beras', CAST(0x44130B00 AS Date))
/****** Object:  Table [dbo].[Planes]    Script Date: 06/22/2020 18:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Planes](
	[IdPlan] [varchar](1) NOT NULL,
	[Descripcion] [varchar](10) NOT NULL,
	[Renta] [money] NOT NULL,
	[CostoMin] [money] NOT NULL,
 CONSTRAINT [PK_Planes] PRIMARY KEY CLUSTERED 
(
	[IdPlan] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Planes] ([IdPlan], [Descripcion], [Renta], [CostoMin]) VALUES (N'1', N'Basico', 500.0000, 4.0000)
INSERT [dbo].[Planes] ([IdPlan], [Descripcion], [Renta], [CostoMin]) VALUES (N'2', N'Medio', 700.0000, 2.0000)
INSERT [dbo].[Planes] ([IdPlan], [Descripcion], [Renta], [CostoMin]) VALUES (N'3', N'Full', 1000.0000, 1.0000)
/****** Object:  Table [dbo].[Telefono]    Script Date: 06/22/2020 18:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Telefono](
	[Telefono] [varchar](10) NOT NULL,
	[IdCliente] [int] NOT NULL,
	[TipoPlan] [varchar](1) NOT NULL,
 CONSTRAINT [PK_Telefono] PRIMARY KEY CLUSTERED 
(
	[Telefono] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Telefono] ([Telefono], [IdCliente], [TipoPlan]) VALUES (N'8095556573', 1005, N'3')
INSERT [dbo].[Telefono] ([Telefono], [IdCliente], [TipoPlan]) VALUES (N'8096670987', 1003, N'2')
INSERT [dbo].[Telefono] ([Telefono], [IdCliente], [TipoPlan]) VALUES (N'8096678967', 1004, N'1')
INSERT [dbo].[Telefono] ([Telefono], [IdCliente], [TipoPlan]) VALUES (N'8098884567', 1001, N'1')
INSERT [dbo].[Telefono] ([Telefono], [IdCliente], [TipoPlan]) VALUES (N'8098889075', 1004, N'2')
INSERT [dbo].[Telefono] ([Telefono], [IdCliente], [TipoPlan]) VALUES (N'8099981290', 1001, N'3')
INSERT [dbo].[Telefono] ([Telefono], [IdCliente], [TipoPlan]) VALUES (N'8099990001', 1002, N'1')
/****** Object:  Table [dbo].[Llamadas]    Script Date: 06/22/2020 18:08:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Llamadas](
	[CodLLamada] [int] IDENTITY(1,1) NOT NULL,
	[Telefono] [varchar](10) NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Duracion] [int] NOT NULL,
 CONSTRAINT [PK_Llamadas] PRIMARY KEY CLUSTERED 
(
	[CodLLamada] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Llamadas] ON
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (1, N'8099990001', CAST(0x0000AB3500000000 AS DateTime), 10)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (2, N'8098884567', CAST(0x0000AA5700000000 AS DateTime), 5)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (3, N'8096670987', CAST(0x0000AA5700000000 AS DateTime), 8)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (4, N'8099981290', CAST(0x0000A9CD00000000 AS DateTime), 12)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (5, N'8098889075', CAST(0x0000AA4200000000 AS DateTime), 20)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (7, N'8096670987', CAST(0x0000AA0B00000000 AS DateTime), 12)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (8, N'8099990001', CAST(0x0000AB1600000000 AS DateTime), 4)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (9, N'8096678967', CAST(0x0000AAA300000000 AS DateTime), 9)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (10, N'8095556573', CAST(0x0000AA6100000000 AS DateTime), 7)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (11, N'8096678967', CAST(0x0000AA5800000000 AS DateTime), 8)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (12, N'8099981290', CAST(0x0000AA8400000000 AS DateTime), 15)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (13, N'8098884567', CAST(0x0000AB2E00000000 AS DateTime), 16)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (14, N'8095556573', CAST(0x0000A9C800000000 AS DateTime), 22)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (15, N'8099981290', CAST(0x0000A9F400000000 AS DateTime), 9)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (16, N'8095556573', CAST(0x0000AA7300000000 AS DateTime), 29)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (17, N'8098889075', CAST(0x0000AA7500000000 AS DateTime), 18)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (18, N'8099990001', CAST(0x0000AA0A00000000 AS DateTime), 23)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (19, N'8099981290', CAST(0x0000AA3E00000000 AS DateTime), 33)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (20, N'8095556573', CAST(0x0000AA5E00000000 AS DateTime), 17)
INSERT [dbo].[Llamadas] ([CodLLamada], [Telefono], [Fecha], [Duracion]) VALUES (21, N'8099990001', CAST(0x0000AB1400000000 AS DateTime), 13)
SET IDENTITY_INSERT [dbo].[Llamadas] OFF
/****** Object:  ForeignKey [FK_Telefono_Cliente]    Script Date: 06/22/2020 18:08:18 ******/
ALTER TABLE [dbo].[Telefono]  WITH CHECK ADD  CONSTRAINT [FK_Telefono_Cliente] FOREIGN KEY([IdCliente])
REFERENCES [dbo].[Cliente] ([IdCliente])
GO
ALTER TABLE [dbo].[Telefono] CHECK CONSTRAINT [FK_Telefono_Cliente]
GO
/****** Object:  ForeignKey [FK_Telefono_Planes]    Script Date: 06/22/2020 18:08:18 ******/
ALTER TABLE [dbo].[Telefono]  WITH CHECK ADD  CONSTRAINT [FK_Telefono_Planes] FOREIGN KEY([TipoPlan])
REFERENCES [dbo].[Planes] ([IdPlan])
GO
ALTER TABLE [dbo].[Telefono] CHECK CONSTRAINT [FK_Telefono_Planes]
GO
/****** Object:  ForeignKey [FK_Llamadas_Telefono]    Script Date: 06/22/2020 18:08:18 ******/
ALTER TABLE [dbo].[Llamadas]  WITH CHECK ADD  CONSTRAINT [FK_Llamadas_Telefono] FOREIGN KEY([Telefono])
REFERENCES [dbo].[Telefono] ([Telefono])
GO
ALTER TABLE [dbo].[Llamadas] CHECK CONSTRAINT [FK_Llamadas_Telefono]
GO
