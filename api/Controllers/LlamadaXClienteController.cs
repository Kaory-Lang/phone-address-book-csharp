using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using midterm2_practice.Context;
using midterm2_practice.Models;

namespace midterm2_practice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LlamadaXClienteController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public LlamadaXClienteController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Llamada
        [HttpGet]
        public async Task<ActionResult<List<Cliente>>> GetLlamadasXCliente()
        {
          if (_context.Llamadas == null || _context.Clientes == null || _context.Telefonos == null)
          {
              return NotFound();
          }

		  List<Cliente> clientes = _context.Clientes.ToList();

		  foreach(Cliente c in clientes) {
			List<Telefono> telefonos =
				_context.Telefonos.Where(x => x.IdCliente == c.IdCliente).ToList();

			c.Telefonos = telefonos;

			if(c.Telefonos.Count > 0) {
			  foreach(Telefono telefono in c.Telefonos) {
				  telefono.Llamada =
					  _context.Llamadas.Where(x => x.Telefono == telefono.Telefono1).ToList();
			  }
			}
		  }

            return clientes;
        }
	}
}
