using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using midterm2_practice.Context;
using midterm2_practice.Models;

namespace midterm2_practice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlaneController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public PlaneController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Plane
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Plane>>> GetPlanes()
        {
          if (_context.Planes == null)
          {
              return NotFound();
          }
            return await _context.Planes.ToListAsync();
        }

        // GET: api/Plane/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Plane>> GetPlane(string id)
        {
          if (_context.Planes == null)
          {
              return NotFound();
          }
            var plane = await _context.Planes.FindAsync(id);

            if (plane == null)
            {
                return NotFound();
            }

            return plane;
        }

        // PUT: api/Plane/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlane(string id, Plane plane)
        {
            if (id != plane.IdPlan)
            {
                return BadRequest();
            }

            _context.Entry(plane).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlaneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Plane
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Plane>> PostPlane(Plane plane)
        {
          if (_context.Planes == null)
          {
              return Problem("Entity set 'DatabaseContext.Planes'  is null.");
          }
            _context.Planes.Add(plane);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PlaneExists(plane.IdPlan))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetPlane", new { id = plane.IdPlan }, plane);
        }

        // DELETE: api/Plane/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlane(string id)
        {
            if (_context.Planes == null)
            {
                return NotFound();
            }
            var plane = await _context.Planes.FindAsync(id);
            if (plane == null)
            {
                return NotFound();
            }

            _context.Planes.Remove(plane);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PlaneExists(string id)
        {
            return (_context.Planes?.Any(e => e.IdPlan == id)).GetValueOrDefault();
        }
    }
}
