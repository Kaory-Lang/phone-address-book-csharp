using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using midterm2_practice.Context;
using midterm2_practice.Models;

namespace midterm2_practice.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LlamadaController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public LlamadaController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/Llamada
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Llamada>>> GetLlamadas()
        {
          if (_context.Llamadas == null)
          {
              return NotFound();
          }
            return await _context.Llamadas.ToListAsync();
        }

        // GET: api/Llamada/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Llamada>> GetLlamada(int id)
        {
          if (_context.Llamadas == null)
          {
              return NotFound();
          }
            var llamada = await _context.Llamadas.FindAsync(id);

            if (llamada == null)
            {
                return NotFound();
            }

            return llamada;
        }

        // PUT: api/Llamada/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLlamada(int id, Llamada llamada)
        {
            if (id != llamada.CodLlamada)
            {
                return BadRequest();
            }

            _context.Entry(llamada).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LlamadaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Llamada
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Llamada>> PostLlamada(Llamada llamada)
        {
          if (_context.Llamadas == null)
          {
              return Problem("Entity set 'DatabaseContext.Llamadas'  is null.");
          }
            _context.Llamadas.Add(llamada);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLlamada", new { id = llamada.CodLlamada }, llamada);
        }

        // DELETE: api/Llamada/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLlamada(int id)
        {
            if (_context.Llamadas == null)
            {
                return NotFound();
            }
            var llamada = await _context.Llamadas.FindAsync(id);
            if (llamada == null)
            {
                return NotFound();
            }

            _context.Llamadas.Remove(llamada);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LlamadaExists(int id)
        {
            return (_context.Llamadas?.Any(e => e.CodLlamada == id)).GetValueOrDefault();
        }
    }
}
